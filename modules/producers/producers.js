HTMLCollection.prototype.forEach = Array.prototype.forEach;
NodeList.prototype.forEach = Array.prototype.forEach;

	let activeButton = 0;
	const producers = document.querySelectorAll(".producers__item");


	producers.forEach((button, index) => {
		if (!button.classList.contains('_active')) {
			button.index = index;
			button.addEventListener('click', addEvent)
		}
	});
	function addEvent() {
		var { index } = this;
		producers.item(activeButton).classList.remove("_active");
		producers.item(activeButton).addEventListener('click', addEvent);
		this.classList.add("_active");
		this.removeEventListener('click', addEvent);
		activeButton = index;
	}
