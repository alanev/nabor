
const counters = document.querySelectorAll(".counter");

[...counters].forEach((counter, index) => {

	const plus = counter.querySelector(".counter__plus");
	const minus = counter.querySelector(".counter__minus");
	const input = counter.querySelector(".counter__input");
	plus.addEventListener('click', function(){
		input.value = parseInt(input.value) + 1;
		console.log(input.value);
	});
	minus.addEventListener('click', function(){
		input.value = parseInt(input.value) - 1;
		console.log(input.value);
	});
});
