HTMLCollection.prototype.forEach = Array.prototype.forEach;
NodeList.prototype.forEach = Array.prototype.forEach;

	let activeButton = 0;
	const buttons = document.querySelectorAll(".carousel-product__mini");
	const items = document.querySelectorAll(".carousel-product__img");


	buttons.forEach((button, index) => {
		if (!button.classList.contains('_active')) {
			button.index = index;
			button.addEventListener('click', addEvent)
		}
	});
	function addEvent() {
		var { index } = this;
		buttons.item(activeButton).classList.remove("_active");
		buttons.item(activeButton).addEventListener('click', addEvent);
		this.classList.add("_active");
		this.removeEventListener('click', addEvent);
		items.item(activeButton).classList.remove("_active");
		items.item(index).classList.add("_active");
		activeButton = index;
	}
