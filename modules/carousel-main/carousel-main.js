var swiper = require('swiper')

new Swiper('.swiper-container', {
	nextButton: '.swiper-button-next',
	prevButton: '.swiper-button-prev',
	setWrapperSize: true,
	slidesPerView: 2,
	spaceBetween: 20
})
