document.addEventListener('DOMContentLoaded', function () {
	require('carousel-product/carousel-product.js');
	require('producers/producers.js');
	require('carousel-main/carousel-main.js');
	require('counter/counter.js');
	require('search/search.js');
});
